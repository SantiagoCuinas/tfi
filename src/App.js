import './App.css';
import Pedido from './pages/pedido'
import Home from './pages/home'
import Productos from './pages/productos'
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/pedido" element={<Pedido />} />
        <Route exact path="/productos" element={<Productos />} />
      </Routes>
    </Router>
  );
}

export default App;
