import * as React from 'react';
import Stack from '@mui/joy/Stack';
import Button from '@mui/joy/Button';
import Input from '@mui/joy/Input';
import SearchIcon from '@mui/icons-material/Search';
import LocalMallIcon from '@mui/icons-material/LocalMall';
import Badge from '@mui/joy/Badge';
import Typography from '@mui/joy/Typography';
import './header.css';
import Link from '@mui/joy/Link';

import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Modal from '@mui/joy/Modal';
import ModalDialog from '@mui/joy/ModalDialog';

export default function Header() {
    const [open, setOpen] = React.useState(false);

    return (
        <div className='header'>
            <Stack direction="row" spacing={2} justifyContent="space-between">
                <div>
                    <Link underline="none" href="/" >
                        <Typography level="h2">regame</Typography>
                    </Link>
                </div>
                <div className='search-container'
                >
                    <Input size="sm" startDecorator={<SearchIcon />} placeholder="Buscar" variant="plain"
                        sx={theme => ({
                            width: '100%'
                        })} />
                </div>
                <Stack spacing={1} direction="row">
                    <Link
                        href="/pedido"
                    >
                        <Button className='button'
                            disabled={false}
                            onClick={function () { }}
                            size="sm"
                            variant="plain"
                            sx={theme => ({
                                color: '#2f2f30'
                            })} >
                            <Badge badgeContent={4}>
                                <LocalMallIcon />
                            </Badge>
                        </Button>
                    </Link>
                    <Link underline="none" href="/productos" >
                        <Button
                            className='button'
                            disabled={false}
                            onClick={function () { }}
                            size="sm"
                            variant="plain"
                            sx={theme => ({
                                color: '#2f2f30'
                            })}
                        >Productos </Button>
                    </Link>
                    <Button
                        className='button'
                        disabled={false}
                        onClick={function () { }}
                        size="sm"
                        variant="plain"
                        sx={theme => ({
                            color: '#2f2f30'
                        })}
                    >Ayuda </Button>
                    <Button
                        className='button-log-in'
                        disabled={false}
                        size="sm"
                        variant="solid"
                        sx={theme => ({
                            color: '#f9efe4',
                            background: '#2f2f30'
                        })}
                        onClick={() => setOpen(true)}
                    >Log in </Button>

                </Stack>
            </Stack>

            <Modal open={open} onClose={() => setOpen(false)}>
                <ModalDialog
                    aria-labelledby="basic-modal-dialog-title"
                    aria-describedby="basic-modal-dialog-description"
                    sx={{ maxWidth: 500, background: '#fdf9f4' }}
                >
                    <Typography id="basic-modal-dialog-title" level="h2">
                        Iniciar sesión
                    </Typography>
                    <form
                        onSubmit={(event) => {
                            event.preventDefault();
                            setOpen(false);
                        }}
                    >
                        <Stack spacing={2}>
                            <FormControl>
                                <FormLabel>Usuario</FormLabel>
                                <Input autoFocus />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Contraseña</FormLabel>
                                <Input type="password" />
                            </FormControl>
                            <Button sx={theme => ({
                                color: '#f9efe4',
                                background: '#2f2f30'
                            })} type="submit">Aceptar</Button>
                        </Stack>
                    </form>
                </ModalDialog>
            </Modal>
        </div>
    )
}