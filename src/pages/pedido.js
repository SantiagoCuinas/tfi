
import Header from '../components/header/header';
import Stack from '@mui/joy/Stack';

import * as React from 'react';
import Button from '@mui/joy/Button';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import CardOverflow from '@mui/joy/CardOverflow';
import Link from '@mui/joy/Link';
import Typography from '@mui/joy/Typography';
import ArrowOutwardIcon from '@mui/icons-material/ArrowOutward';

export default function Pedidos() {
  return (
    <div className="home">
      <Header />
      <Stack direction="row"
        justifyContent="center"
        spacing={6}
        alignItems="flex-start"
        sx={{
          margin: '4%',
        }}
      >
        <Stack direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={6}>
          <Stack direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={6}>
            {createCard()}
            {createCard()}
            {createCard()}
          </Stack>
          <Stack direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={6}>
            {createCard()}
            {createCard('hidden')}
            {createCard('hidden')}
          </Stack>
        </Stack>
        <Stack direction="column"
          justifyContent="center"
          alignItems="flex-start"
          spacing={4}
          sx={{
            width: '70%',
          }}
        >
          <Stack direction="column"
            justifyContent="center"
            alignItems="flex-start"
            spacing={0}
            sx={{
              width: 300,
            }}
          >
            <Typography level="h2">
              Resumen
            </Typography>
          </Stack>
          <Stack direction="row"
            justifyContent="space-between"
            alignItems="flex-start"
            spacing={0}
            sx={{
              width: 300,
            }}
          >
            <Typography level="h5">
              Productos (4)
            </Typography>
            <Typography level="h5">
              100.000
            </Typography>
          </Stack>
          <Stack direction="row"
            justifyContent="space-between"
            alignItems="flex-start"
            spacing={0}
            sx={{
              width: 300,
            }}
          >
            <Typography level="h5">
              Envío
            </Typography>
            <Typography level="h5">
              5.000
            </Typography>
          </Stack>
          <Stack direction="row"
            justifyContent="space-between"
            alignItems="flex-start"
            spacing={0}
            sx={{
              width: 300,
            }}
          >
            <Typography level="h4">
              Total
            </Typography>
            <Typography level="h4">
              105.000
            </Typography>
          </Stack>
          <Button sx={theme => ({
            color: '#f9efe4',
            background: '#2f2f30',
            width: '100%'
          })} type="submit">Pedir</Button>
        </Stack>
      </Stack>
    </div>
  );
}

function createCard(visibility) {
  return <Card sx={{ maxWidth: '100%', boxShadow: 'lg', background: '#fdf9f4', visibility }}>
    <CardOverflow>
      <img
        src="https://logg.api.cygnus.market/static/logg/Global/Motherboard%20Gigabyte%20H610M%20H%20DDR4%20Intel%2012%C2%AA%20LGA1700/93e770d7eacc4061b41303c6ad05b1d2.webp"
        srcSet="https://logg.api.cygnus.market/static/logg/Global/Motherboard%20Gigabyte%20H610M%20H%20DDR4%20Intel%2012%C2%AA%20LGA1700/93e770d7eacc4061b41303c6ad05b1d2.webp"
        loading="lazy"
        alt=""
        style={{ width: '100%' }}
      />
    </CardOverflow>
    <CardContent>
      <Link
        href="#product-card"
        fontWeight="md"
        color="neutral"
        textColor="text.primary"
        overlay
        endDecorator={<ArrowOutwardIcon />}
      >
        GIGABYTE H610M H
      </Link>
      <Typography
        level="title-lg"
        sx={{ mt: 1, fontWeight: 'xl' }}
      >
        $ 120.000
      </Typography>
    </CardContent>
    <CardOverflow>
      <Button variant="solid" sx={theme => ({
        color: '#f9efe4',
        background: '#2f2f30'
      })} size="lg">
        Quitar</Button>
  </CardOverflow>
  </Card >
}
