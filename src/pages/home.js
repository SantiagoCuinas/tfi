import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import Header from '../components/header/header';
import Stack from '@mui/joy/Stack';
import image from '../imgs/hero.svg'; // Tell webpack this JS file uses this image
import Link from '@mui/joy/Link';

export default function Home() {
  return (
    <div className="home">
      <Header />
      <Stack direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={6}>
        <div className="hero-h1-container">
          <Typography level="h1"
            sx={theme => ({
              fontSize: '60px',
              textAlign: 'center'
            })}>
            La solución más económica para subir de nivel tu computadora
          </Typography>
        </div>
        <div className="hero-h2-container">
          <Typography level="h2" sx={{textAlign: 'center'}}>
            Los componentes reacondicionados de regame son la mejor manera de actualizar tu hardware a una fracción del costo.
          </Typography>
        </div>
        <Link underline="none" href="/productos" >
        <Button
          disabled={false}
          onClick={function () { }}
          size="lg"
          variant="solid"
          sx={theme => ({
            color: '#f9efe4',
            background: '#2f2f30'
          })}
        >Conoce nuestras opciones</Button>
        </Link>
        <img src={image} alt="Personas en un sillón festejando" className="hero-image" />
      </Stack>
    </div>
  );
}
