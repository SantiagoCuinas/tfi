
import Header from '../components/header/header';
import Stack from '@mui/joy/Stack';

import * as React from 'react';
import Button from '@mui/joy/Button';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import CardOverflow from '@mui/joy/CardOverflow';
import Link from '@mui/joy/Link';
import Typography from '@mui/joy/Typography';
import ArrowOutwardIcon from '@mui/icons-material/ArrowOutward';
import Pagination from '@mui/material/Pagination';
import Dropdown from '@mui/joy/Dropdown';
import Menu from '@mui/joy/Menu';
import MenuButton from '@mui/joy/MenuButton';
import MenuItem from '@mui/joy/MenuItem';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import Slider from '@mui/joy/Slider';

export default function Productos() {
  const [value, setValue] = React.useState([20, 37]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="home">
      <Header />
      <Stack direction="row"
        justifyContent="center"
        spacing={6}
        alignItems="flex-start"
        sx={{
          margin: '4%',
        }}
      >
        <Stack direction="column"
          justifyContent="center"
          alignItems="flex-start"
          spacing={4}
          sx={{
            width: '70%',
          }}
        >
          <Stack direction="column"
            justifyContent="center"
            alignItems="flex-start"
            spacing={0}
            sx={{
              width: 300,
            }}
          >
            <Typography level="h2">
              h610m
            </Typography>
            <Typography level="h5">
              35 Resultados
            </Typography>
          </Stack>
          <Stack direction="column"
            justifyContent="center"
            alignItems="flex-start"
            spacing={0}
            sx={{
              width: 300,
            }}
          >
            <Typography level="h5">
              Precio
            </Typography>
            <Slider
              getAriaLabel={() => 'Temperature range'}
              value={value}
              onChange={handleChange}
              valueLabelDisplay="auto"
              getAriaValueText={valueText}
            />
          </Stack>
          <Dropdown>
            <MenuButton sx={{ paddingLeft: 0 }} variant="plain" endDecorator={<KeyboardArrowDownIcon />}
            >Categorías</MenuButton>
            <Menu>
              <MenuItem>Cat 1</MenuItem>
              <MenuItem>Cat 2</MenuItem>
              <MenuItem>Cat 3</MenuItem>
            </Menu>
          </Dropdown>
          <Dropdown>
            <MenuButton sx={{ paddingLeft: 0 }} variant="plain" endDecorator={<KeyboardArrowDownIcon />}
            >Estado</MenuButton>
            <Menu>
              <MenuItem>A</MenuItem>
              <MenuItem>B</MenuItem>
              <MenuItem>C</MenuItem>
            </Menu>
          </Dropdown>
        </Stack>
        <Stack direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={6}>
          <Stack direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={6}>
            {createCard()}
            {createCard()}
            {createCard()}
            {createCard()}
          </Stack>
          <Stack direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={6}>
            {createCard()}
            {createCard()}
            {createCard()}
            {createCard()}
          </Stack>
          <Pagination count={10} shape="rounded" />
        </Stack>
      </Stack>
    </div>
  );
}

function createCard() {
  return <Card sx={{ maxWidth: '100%', boxShadow: 'lg', background: '#fdf9f4' }}>
    <CardOverflow>
      <img
        src="https://logg.api.cygnus.market/static/logg/Global/Motherboard%20Gigabyte%20H610M%20H%20DDR4%20Intel%2012%C2%AA%20LGA1700/93e770d7eacc4061b41303c6ad05b1d2.webp"
        srcSet="https://logg.api.cygnus.market/static/logg/Global/Motherboard%20Gigabyte%20H610M%20H%20DDR4%20Intel%2012%C2%AA%20LGA1700/93e770d7eacc4061b41303c6ad05b1d2.webp"
        loading="lazy"
        alt=""
        style={{ width: '100%' }}
      />
    </CardOverflow>
    <CardContent>
      <Link
        href="#product-card"
        fontWeight="md"
        color="neutral"
        textColor="text.primary"
        overlay
        endDecorator={<ArrowOutwardIcon />}
      >
        GIGABYTE H610M H
      </Link>
      <Typography
        level="title-lg"
        sx={{ mt: 1, fontWeight: 'xl' }}
      >
        $ 120.000
      </Typography>
    </CardContent>
    <CardOverflow>
      <Button variant="solid" sx={theme => ({
        color: '#f9efe4',
        background: '#2f2f30'
      })} size="lg">
        Agregar al pedido
      </Button>
    </CardOverflow>
  </Card>
}

function valueText(value) {
  return `${value}°C`;
}
